/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#include "../MdtCablingJsonDumpAlg.h"
#include "../MuonABLineJsonDumpAlg.h"

DECLARE_COMPONENT(MdtCablingJsonDumpAlg)
DECLARE_COMPONENT(MuonABLineJsonDumpAlg)